extends Area2D

var speed = 250
onready var min_height = position.y - 300
onready var max_height = position.y + 300

var up = false

func _ready():
	print(position)

func _physics_process(delta):
	if position.y <= min_height:
		up = false
	elif position.y > max_height:
		up = true
	if up:
		position.y -= (speed * delta)
	else:
		position.y += (speed * delta)
	

func _on_Mobs_body_entered(body):
	if body.name == "Player":
		body.die()
