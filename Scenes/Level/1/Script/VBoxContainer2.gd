extends VBoxContainer

func _ready():
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://Fonts/Roboto-Black.ttf")
	dynamic_font.size = 120
	dynamic_font.outline_size = 5
	dynamic_font.outline_color = Color( 0, 0, 0, 1 )
	dynamic_font.use_filter = true
	var label = Label.new()
	label.text = "For every 50 dies,\n you can click on Spike to remove them,\n but every level only limited to 3 Spike"
	label.rect_size.x = 320
	label.rect_size.y = 200
	label.align = VALIGN_CENTER
	label.add_font_override("font", dynamic_font)
	label.add_color_override("font_color", Color.red)
	self.add_child(label)

	

