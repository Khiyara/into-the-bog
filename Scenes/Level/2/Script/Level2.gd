extends Node2D

onready var player = get_node("/root/Player")
onready var scene_switcher = self.get_node("/root/Global")
onready var hud = get_node("/root/Hud")

onready var player_die_this_level = 0

var click_limit = 3
var click_count = 0

func _ready():
	var last_checkpoint = scene_switcher.get_param("last_checkpoint")
	var last_scene = scene_switcher.get_param("last_scene")
	if last_checkpoint and last_scene == self.name:
		print("Setting player position based on checkpoint" + str(last_checkpoint))
		player.position = last_checkpoint
	else:
		print("Setting player position based on spawn point level" + str($SpawnPoint.position))
		player.position = $SpawnPoint.position
	player.last_checkpoint = scene_switcher.get_param("Level2")
	self.get_node("/root/Hud").set_text_level(player_die_this_level)
	self.get_node("/root/Hud").get_node("VBoxContainer/Sprite4/Skill").setSprite(click_limit)
	self.get_node("/root/Hud").set_text(player.die_count)

func _input(event):
	if event is InputEventMouseButton:
		if player_die_this_level >= 50 * (click_count + 1)  and click_limit > 0 and $Spike.get_cellv($Spike.world_to_map(get_global_mouse_position())) != -1:
			$Spike.set_cellv($Spike.world_to_map(get_global_mouse_position()), TileMap.INVALID_CELL)
			click_limit -= 1
			click_count += 1
			self.get_node("/root/Hud").get_node("VBoxContainer/Sprite4/Skill").setSprite(click_limit)

func die_this_level():
	player_die_this_level += 1
	print("Player die level 2: " + str(player_die_this_level))
	self.get_node("/root/Hud").set_text(player.die_count)
	self.get_node("/root/Hud").set_text_level(player_die_this_level)
	return player_die_this_level
