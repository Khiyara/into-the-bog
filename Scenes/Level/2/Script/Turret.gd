extends Node2D

onready var sword = preload("res://Scenes/Level/2/Sword.tscn")
const TIME_SHOOT = 1

var time = 0

func _ready():
	pass # Replace with function body.
	

func shoot():
	var swd = sword.instance()
	self.add_child(swd)

func _process(delta):
	time += delta
	if time > TIME_SHOOT:
		time = 0
		shoot()
