extends Area2D 

export (int) var direction = 0

var speed = 800
var velocity = Vector2()

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position.x += speed * delta	

func _on_Sword_body_entered(body):
	if body.name == "Player":
		body.die()
	if body.name == "TileMap":
		queue_free()
	if body.name == "Spike":
		queue_free()
