extends Node2D

var time = 0
onready var player = get_node("/root/Player")
onready var scene_switcher = self.get_node("/root/Global")
onready var hud = get_node("/root/Hud")
onready var player_die_this_level = 0

func _ready():
	print("Setting player position based on spawn point level" + str($SpawnPoint.position))
	player.position = $SpawnPoint.position
	player.last_checkpoint = null
	self.get_node("/root/Hud").get_node("VBoxContainer/Sprite4/Skill").setSprite(0)
	self.get_node("/root/Hud").set_text_level(player.boss_level_1_die)

func shoot_laser():
	var laser_random_spawn = [$LaserSpawnPoint, $LaserSpawnPoint2, $LaserSpawnPoint3, $LaserSpawnPoint4]
	var random_index = randi() % 4
	laser_random_spawn[random_index].shoot()
		
func spray_sword():
	var sword_random_spawn = [$SwordRain, $SwordRain2, $SwordRain3, $SwordRain4, $SwordRain5, $SwordRain6, $SwordRain7, $SwordRain8, $SwordRain9, $SwordRain10, $SwordRain11, $SwordRain12, $SwordRain13, $SwordRain14, $SwordRain15, $SwordRain16, $SwordRain17, $SwordRain18, $SwordRain19, $SwordRain20, $SwordRain21, $SwordRain22, $SwordRain23, $SwordRain24, $SwordRain25, $SwordRain26, $SwordRain27, $SwordRain28, $SwordRain29, $SwordRain30, $SwordRain31, $SwordRain32, $SwordRain33, $SwordRain34, $SwordRain35, $SwordRain36, $SwordRain37]
	var random_index = randi() % 37
	sword_random_spawn[random_index].rain()
	
func die_this_level():
	player_die_this_level += 1
	print("Player die boss 1: " + str(player_die_this_level))
	self.get_node("/root/Hud").set_text(player.die_count)
	self.get_node("/root/Hud").set_text_level(player_die_this_level)
	return player_die_this_level
	
