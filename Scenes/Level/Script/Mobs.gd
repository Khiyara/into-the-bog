extends Node2D

onready var sword = preload("res://Scenes/Boss/LastBoss/Sword.tscn")

func _ready():
	position.x = self.get_node("/root/Player").position.x

func _process(delta):
	position.x = self.get_node("/root/Player").position.x
	
func shoot():
	var swd = sword.instance()
	get_parent().add_child(swd)
	swd.global_position = self.global_position
