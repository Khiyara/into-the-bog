extends Node2D

onready var player = get_node("/root/Player")
onready var scene_switcher = self.get_node("/root/Global")
onready var hud = get_node("/root/Hud")
onready var ball = preload("res://Scenes/Boss/LastBoss/Ball.tscn")
onready var player_die_this_level = 0

var nodes = []
var nodes_active = []

func _ready():
	print("Setting player position based on spawn point level" + str($SpawnPoint.position))
	player.position = $SpawnPoint.position
	player.last_checkpoint = null
	self.get_node("/root/Hud").get_node("VBoxContainer/Sprite4/Skill").setSprite(0)
	self.get_node("/root/Hud").set_text_level(player.boss_level_2_die)
	for i in range(1, 36):
		var name = "Nodes" + str(i)
		nodes.append(name)
		nodes_active.append(true)
		get_node(nodes[i-1]).hide()
		get_node(nodes[i-1]).position = Vector2(rand_range(0, 1000), rand_range(-1500, 1500))

func die_this_level():
	player.set_boss_level_2_die(player.boss_level_2_die + 1)
	print("Player die last boss: " + str(player.boss_level_2_die))
	self.get_node("/root/Hud").set_text(player.die_count)
	self.get_node("/root/Hud").set_text_level(player.boss_level_2_die)
	return player.boss_level_2_die

func shoot_nodes():
	for i in range(len(nodes)):
		get_node(nodes[i]).show()
		get_node(nodes[i]).get_node("Sprite").show()
	
	var random_index = rand_range(0, 35)
	while not nodes_active[random_index]:
		random_index = rand_range(0, 35)
	var bll = ball.instance()
	nodes_active[random_index] = false
	self.get_node(nodes[random_index]).add_child(bll)
	bll.set_angle(player)
	self.get_node(nodes[random_index]).get_node("Sprite").hide()
	
func clear_nodes():
	for i in range(len(nodes)):
		var node = get_node(nodes[i])
		node.hide()
		for j in range(node.get_child_count()):
			var node_ball = node.get_child(j)
			if "Ball" in node_ball.name:
				node_ball.queue_free()
				break
		nodes_active[i] = true
