extends Node2D

onready var player = get_node("/root/Player")
onready var scene_switcher = self.get_node("/root/Global")
onready var hud = get_node("/root/Hud")

func _ready():
	var last_checkpoint = scene_switcher.get_param("last_checkpoint")
	var last_scene = scene_switcher.get_param("last_scene")
	print("Last Scene: " + last_scene)
	print("Last Checkpoint: " + str(last_checkpoint))
	if last_checkpoint and last_scene == self.name:
		print("Setting player position based on checkpoint" + str(last_checkpoint))
		player.position = last_checkpoint
	else:
		print("Setting player position based on spawn point level" + str($SpawnPoint.position))
		player.position = $SpawnPoint.position
	player.last_checkpoint = scene_switcher.get_param("Level4")
	self.get_node("/root/Hud").set_text_level("0")
	self.get_node("/root/Hud").get_node("VBoxContainer/Sprite4/Skill").setSprite(0)
	self.get_node("/root/Hud").set_text(player.die_count)
