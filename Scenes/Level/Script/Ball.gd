extends Area2D

var direction = Vector2(0,0)
var speed = 800

func _ready():
	pass

func set_angle(player):
	var dir = (player.global_position - get_parent().global_position).normalized()
	global_rotation = dir.angle() + PI / 2.0
	direction = dir

func _physics_process(delta):
	position += direction * speed * delta
	if position.x <= -4000:
		queue_free()


func _on_Ball_body_entered(body):
	if body.name == "Player":
		body.die()
