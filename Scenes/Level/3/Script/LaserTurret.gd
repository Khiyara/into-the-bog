extends Node2D

onready var laser = preload("res://Scenes/Level/3/Laser.tscn")
const TIME_SHOOT = 9

var time = rand_range(0, 4.5)

func _ready():
	pass # Replace with function body.
	

func shoot():
	var lsr = laser.instance()
	if $Sprite.flip_h == true:
		lsr.direction = -1
		lsr.get_node("Sprite").flip_h = true
	self.add_child(lsr)

func _process(delta):
	time += delta
	if time > TIME_SHOOT:
		time = rand_range(0, 4.5)
		shoot()
