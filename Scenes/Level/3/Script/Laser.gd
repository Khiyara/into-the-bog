extends Area2D 

export (int) var direction = 1

var speed = 500
var velocity = Vector2()

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position.x += speed * delta	* direction

func _on_Laser_body_entered(body):
	if body.name == "Player":
		body.die()
		queue_free()
	if body.name == "TileMap":
		queue_free()
