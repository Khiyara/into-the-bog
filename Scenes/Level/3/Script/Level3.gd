extends Node2D

const TIME_PERIOD = 0.05
onready var player = get_node("/root/Player")
onready var scene_switcher = self.get_node("/root/Global")
onready var hud = get_node("/root/Hud")
onready var canvas_modulate = self.get_node("CanvasModulate")

onready var player_die_this_level = 0

var click_limit = 3
var click_count = 0

var time = 0
var flip_lasser_turrets = ["LaserTurret3", "LaserTurret5", "LaserTurret6", "LaserTurret7", "LaserTurret8"]

func going_dark(delta):
	time += delta
	if time > TIME_PERIOD:
		canvas_modulate.color.a -= 0.01
		time = 0

func _ready():
	var last_checkpoint = scene_switcher.get_param("last_checkpoint")
	var last_scene = scene_switcher.get_param("last_scene")
	print("Last Scene: " + last_scene)
	print("Last Checkpoint: " + str(last_checkpoint))
	if last_checkpoint and last_scene == self.name:
		print("Setting player position based on checkpoint" + str(last_checkpoint))
		player.position = last_checkpoint
	else:
		print("Setting player position based on spawn point level" + str($SpawnPoint.position))
		player.position = $SpawnPoint.position
	player.last_checkpoint = scene_switcher.get_param("Level3")
	self.get_node("/root/Hud").set_text_level(player_die_this_level)
	self.get_node("/root/Hud").get_node("VBoxContainer/Sprite4/Skill").setSprite(click_limit)
	self.get_node("/root/Hud").set_text(player.die_count)
	for turret in flip_lasser_turrets:
		self.get_node(turret).get_node("Sprite").flip_h = true

func _process(delta):
	if canvas_modulate.color.a > 0:
		going_dark(delta)
	
func _input(event):
	if event is InputEventMouseButton:
		if player_die_this_level >= 50 * (click_count + 1)  and click_limit > 0 and $Spike.get_cellv($Spike.world_to_map(get_global_mouse_position())) != -1:
			$Spike.set_cellv($Spike.world_to_map(get_global_mouse_position()), TileMap.INVALID_CELL)
			click_limit -= 1
			click_count += 1
			self.get_node("/root/Hud").get_node("VBoxContainer/Sprite4/Skill").setSprite(click_limit)

func die_this_level():
	player_die_this_level += 1
	print("Player die level 3: " + str(player_die_this_level))
	self.get_node("/root/Hud").set_text(player.die_count)
	self.get_node("/root/Hud").set_text_level(player_die_this_level)
	for i in range(get_child_count()):
		var node = get_child(i)
		if "LaserTurret" in node.name:
			for j in range(node.get_child_count()):
				if not "Sprite" in node.get_child(j).name:
					node.get_child(j).queue_free()
	return player_die_this_level
	
