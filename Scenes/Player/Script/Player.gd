extends KinematicBody2D

export (int) var speed = 900
export (int) var GRAVITY = 2600
export (int) var jump_speed = -900

const Bullet = preload("res://Scenes/Player/Bullet.tscn")
const Flashlight = preload("res://Scenes/Player/Flashlight.tscn")

onready var animator = self.get_node("Animation")
onready var sprite = self.get_node("Sprite")
onready var sprite_default_position = sprite.position

onready var scene_switcher = get_node("/root/Global")

const UP = Vector2(0,-1)

var velocity = Vector2()
var direction = 0
var already_double_jump = false
var last_checkpoint = null
var flashing = false
var flashlight = null
var die_count = 0
var boss_level_1_die = 0
var boss_level_2_die = 0
var pause = false
var freeze = false

func shooting():
	self.get_node("Shoot").play()
	var bullet = Bullet.instance()
	bullet.direction = direction
	bullet.name = "Bullet"
	get_parent().add_child(bullet)
	bullet.global_position = $CollisionShape2D.global_position
	bullet.global_position.x += (direction * 120)

func flashlight():
	if not flashing:
		print("Nyalain Senter")
		flashing = true
		var flashlight_instance = Flashlight.instance()
		flashlight_instance.name = "Flashlight"
		flashlight = flashlight_instance
		self.add_child(flashlight)
	elif flashing:
		print("Matiin Senter")
		flashing = false
		for i in range(self.get_child_count()):
			if self.get_child(i).name == "Flashlight":
				self.get_child(i).queue_free()
				break
		flashlight = null

func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed("ui_up") and not already_double_jump:
		self.get_node("Jump").play()
		velocity.y = jump_speed
		already_double_jump = true
	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		self.get_node("Jump").play()
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		
		velocity.x += speed
		direction = 1
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		direction = -1
	if Input.is_action_just_pressed("shoot"):
		shooting()
	if Input.is_action_just_pressed("ui_down"):
		flashlight()

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	if not freeze:
		get_input()
	if is_on_floor():
		already_double_jump = false
	velocity = move_and_slide(velocity, UP)
	self.get_node("TextEdit").set_text(str(die_count))
	check_collision()

func die():
	self.get_node("Die").play()
	if get_tree().get_current_scene().name == "Boss1":
		set_boss_level_1_die(boss_level_1_die + 1)
	if get_tree().get_current_scene().name == "Boss2":
		set_boss_level_1_die(boss_level_2_die + 1)
	die_count += 1
	print("Die Count: " + str(die_count))
	if last_checkpoint != null:
		position = last_checkpoint
	else:
		print("Change Scene Because Die and not have checkpoint")
		var last_scene = scene_switcher.get_param("last_scene")
		if last_scene != null:
			scene_switcher.goto_scene("res://Scenes/Level/" + last_scene + ".tscn")
		else:
			position = scene_switcher.position
	get_tree().get_current_scene().die_this_level()
	return die_count

func freeze():
	freeze = true
	return freeze
	
func unfreeze():
	freeze = false
	return freeze

func check_collision():
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.name == "Spike":
			die()
			break
			
func set_boss_level_1_die(num):
	boss_level_1_die = num

func set_boss_level_2_die(num):
	boss_level_2_die = num

func _process(delta):
	if velocity.x != 0:
		animator.play("Run")
		if velocity.x > 0:
			sprite.flip_h = false
			sprite.position.x = sprite_default_position.x
		else:
			sprite.flip_h = true
			sprite.position.x = sprite_default_position.x - 12
	else:
		animator.play("Idle")

func save():
	var coordinate = scene_switcher.get_param("last_checkpoint")
	var x = null
	var y = null
	if coordinate != null:
		x = coordinate.x
		y = coordinate.y
	var save_dict = {
		"name": "Player",
		"filename": get_filename(),
		"die": die_count,
		"die_boss_1": boss_level_1_die,
		"last_checkpoint": last_checkpoint,
		"last_scene": get_tree().get_current_scene().name,
		"x": x, 
		"y": y
	}
	return save_dict
