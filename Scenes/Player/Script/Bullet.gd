extends KinematicBody2D 

export (int) var direction = 0
var velocity = Vector2()
var speed = 500

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	if (direction < 0):
		$Sprite.flip_h = true
	velocity.x += (speed * direction)
	velocity = move_and_slide(velocity)
	check_collision()

func check_collision():
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.name == "Spike":
			print("Hit Spike")
		if collision.collider.name == "Boss" or collision.collider.name == "LastBoss":
			print(collision.collider.decrease_health())
		queue_free()
