extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# 1 = (164, 676, 47, 67)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func setSprite(num):
	if num == 0:
		self.region_rect = Rect2(164, 804, 54, 69)
	elif num == 1:
		self.region_rect = Rect2(164, 676, 47, 67)
	elif num == 2:
		self.region_rect = Rect2(164, 549, 57, 68)
	elif num == 3:
		self.region_rect = Rect2(164, 420, 51, 69)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
