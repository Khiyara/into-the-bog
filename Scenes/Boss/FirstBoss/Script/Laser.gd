extends Area2D 

export (int) var direction = 0

var speed = 1300
var velocity = Vector2()

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position.x += speed * delta
	if position.x > 5500:
		queue_free()

func _on_Laser_body_entered(body):
	if body.name == "Player":
		body.die()
