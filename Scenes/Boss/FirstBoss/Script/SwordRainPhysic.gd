extends Area2D 

var speed = 1000
var velocity = Vector2()

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position.y += speed * delta

func _on_SwordRain_body_entered(body):
	if body.name == "Player":
		body.die()
	if body.name == "TileMap":
		queue_free()
