extends KinematicBody2D

const DASH_PERIOD = 16
const IDLE_PERIOD = 6
const BULLET_HELL_PERIOD = 1.25
const LASER_PERIOD = 0.5
const SPRAY_PERIOD = 0.035

enum mode {
	IDLE,
	BULLET,
	SPRAY,
	LASER,
	DASH,
	DIE
}

const bullet_hell = preload("res://Scenes/Boss/FirstBoss/BulletHell.tscn")

export (int) var speed = 1200
export (int) var health = 500
var max_health = health

var time = 0
var time_idle = 0
var time_bullet_hell = 0
var time_laser = 0
var time_spray = 0
var die = false
var right = randi() % 2
var direction = Vector2(0, 0)

var hit_wall = false
var on_floor = false

export (mode) var boss_mode = mode.IDLE

func action(delta, right):
	match boss_mode:
		mode.IDLE:
			hit_wall = false
			position.x = 0
			position.y = 0
			boss_mode = mode.values()[1 + (randi() % 3)]
		mode.BULLET:
			time_bullet_hell += delta
			hit_wall = false
			if time_bullet_hell > BULLET_HELL_PERIOD:
				shoot_bullet_hell()
				time_bullet_hell = 0
			position.x = 0
			position.y = 0
			direction = move_and_slide(direction)
		mode.SPRAY:
			time_spray += delta
			hit_wall = false
			if time_spray > SPRAY_PERIOD:
				spray_sword()
				time_spray = 0
			position.x = 0
			position.y = 0
			direction = move_and_slide(direction)
		mode.LASER:
			time_laser += delta
			hit_wall = false
			if time_laser > LASER_PERIOD:
				shoot_laser()
				time_laser = 0
			position.x = 0
			position.y = 0
			direction = move_and_slide(direction)
		mode.DASH:
			if hit_wall:
				direction.y += (speed * delta)
				direction = move_and_slide(direction)
			elif right:
				self.get_node("Sprite").flip_h = false
				direction.x += (speed * delta)
				direction.y += (400 * delta)
				direction = move_and_slide(direction)
			elif not right:
				self.get_node("Sprite").flip_h = true
				direction.x -= (speed * delta)
				direction.y += (400 * delta)
				direction = move_and_slide(direction)
			if position.y > 1250:
				if right:
					position.x -= (speed * 2 * delta)
				else:
					position.x += (speed * 2 * delta)
		mode.DIE:
			die = true
	check_collision()
	
func shoot_bullet_hell():
	for i in range(40):
		var bullet = bullet_hell.instance()
		bullet.set_angle()
		get_parent().add_child(bullet)

func shoot_laser():
	get_parent().shoot_laser()

func spray_sword():
	get_parent().spray_sword()
	
func check_collision():
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider != null:
			if collision.collider.name == "TileMap":
				hit_wall = true
			elif collision.collider.name == "Player":
				var player = collision.collider
				player.die()
				break
  
func decrease_health():
	health -= 10
	$ProgressBar.max_value = 100
	$ProgressBar.value = int((health * 100) / max_health)
	if health <= 0:
		win()
	return health

func win():
	var next_level = get_parent().get_node("NextLevel")
	next_level.visible = true
	next_level.position = Vector2(-1961.07, 1031.87)
	position = Vector2(-10000,-10000)
	$Sprite.visible = false
	boss_mode = mode.DIE

func _ready():
	pass # Replace with function body.

func _process(delta):
	if die:
		return
	time += delta
	time_idle += delta
	action(delta, right)
	if time_idle > IDLE_PERIOD:
		time_idle = 0
		boss_mode = mode.values()[1 + (randi() % 3)]
	if time > DASH_PERIOD:
		time = 0
		time_idle = 0
		boss_mode = mode.DASH
		right = randi() % 2
		
	
	
