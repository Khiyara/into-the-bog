extends Area2D 

var direction = Vector2()
var velocity = Vector2()

var speed = 700

func set_angle():
	var rng = [rand_range(-100,100), rand_range(0,100)]
	direction = Vector2(rng[0],rng[1]).normalized()
	rotation = direction.angle()

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position += direction * speed * delta

func _on_BulletHell_body_entered(body):
	if body.name == "Player":
		body.die()
	if body.name == "TileMap":
		queue_free()
