extends Area2D 

var direction = Vector2()
var velocity = Vector2()

var speed = 700

func set_angle():
	var rng = [rand_range(-75, -25), rand_range(-100,100)]
	direction = Vector2(rng[0],rng[1]).normalized()
	rotation = direction.angle()

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position += direction * speed * delta
	if position.x <= -3000 or position.y > 3000 or position.y < -2000:
		queue_free()

func _on_BulletSpray_body_entered(body):
	if body.name == "Player":
		body.die()
