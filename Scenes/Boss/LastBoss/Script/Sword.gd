extends Area2D

var speed = 1000
var velocity = Vector2()

func _physics_process(delta):
	position.y += speed * delta
	if position.y >= 1500:
		queue_free()

func _on_Sword_body_entered(body):
	if body.name == "Player":
		body.die()
