extends Area2D 

export (int) var direction = -1

var speed = 800
var velocity = Vector2()

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position.x += (speed * direction * delta)
	if position.x < -5500:
		queue_free()

func _on_Ship_body_entered(body):
	if body.name == "Player":
		body.die()
