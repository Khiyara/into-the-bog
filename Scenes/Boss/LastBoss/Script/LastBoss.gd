extends KinematicBody2D

onready var bullet = preload("res://Scenes/Boss/LastBoss/Bullet.tscn")
onready var ship = preload("res://Scenes/Boss/LastBoss/Ship.tscn")
onready var bullet_spray = preload("res://Scenes/Boss/LastBoss/BulletSpray.tscn")

export (int) var speed = 440

const IDLE_PERIOD = 12
const BULLET_PERIOD = 0.65
const SHIP_PERIOD = 0.9
const NODES_PERIOD = 0.4
const SPRAY_PERIOD = 1.5
const RAIN_PERIOD = 0.65

var time = 0
var time_bullet_hell = 0
var time_spray = 0
var time_node = 0
var time_ship = 0
var time_rain = 0
var up = false
var health = 750
var max_health = health
var die = false
var lasering = false
var direction = Vector2(0, 0)

enum mode {
	IDLE,
	BULLET,
	SHIP,
	SPRAY,
	NODE,
	RAIN,
	DIE
}

export (mode) var boss_mode = mode.IDLE

func action(delta):
	match boss_mode:
		mode.IDLE:
			position.y = 0
			boss_mode = mode.values()[1 + (randi() % 5)]
		mode.BULLET:
			time_bullet_hell += delta
			if time_bullet_hell > BULLET_PERIOD:
				time_bullet_hell = rand_range(0, BULLET_PERIOD)
				shoot()
			if position.y > 800:
				up = true
			elif position.y <= -1200:
				up = false
			if up:
				position.y -= (speed * 2 * delta)
			else:
				position.y += (speed * 2 * delta)
		mode.SHIP:
			time_ship += delta
			if time_ship > SHIP_PERIOD:
				time_ship = rand_range(0, SHIP_PERIOD)
				ship()
			if position.y > 800:
				up = true
			elif position.y <= -1200:
				up = false
			if up:
				position.y -= (speed * 1.2 * delta)
			else:
				position.y += (speed * 1.2 * delta)
		mode.SPRAY:
			time_spray += delta
			if time_spray > SPRAY_PERIOD:
				time_spray = 0
				spray()
			if position.y > 800:
				up = true
			elif position.y <= -1200:
				up = false
			if up:
				position.y -= (speed * delta)
			else:
				position.y += (speed * delta)
		mode.NODE:
			time_node += delta
			if time_node > NODES_PERIOD:
				time_node = 0
				nodes()
			if position.y > 800:
				up = true
			elif position.y <= -1200:
				up = false
			if up:
				position.y -= (speed * delta)
			else:
				position.y += (speed * delta)
		mode.RAIN:
			time_rain += delta
			if time_rain > RAIN_PERIOD:
				time_rain = 0
				rain()
			if position.y > 800:
				up = true
			elif position.y <= -1200:
				up = false
			if up:
				position.y -= (speed * delta)
			else:
				position.y += (speed * delta)
		mode.DIE:
			die = true
	check_collision()

func shoot():
	var blt = bullet.instance()
	get_parent().add_child(blt)
	blt.global_position = $CollisionShape2D.global_position
	
func ship():
	var chn = ship.instance()
	self.add_child(chn)

func rain():
	get_parent().get_node("Mobs").shoot()

func nodes():
	get_parent().shoot_nodes()

func spray():
	for i in range(20):
		var swd = bullet_spray.instance()
		swd.set_angle()
		swd.global_position = $CollisionShape2D.global_position
		get_parent().add_child(swd)
		
func decrease_health():
	health -= 10
	$ProgressBar.max_value = 100
	$ProgressBar.value = int((health * 100) / max_health)
	if health <= 0:
		win()
	return health

func win():
	var next_level = get_parent().get_node("NextLevel")
	next_level.visible = true
	next_level.position = Vector2(1000, 500)
	position = Vector2(-10000,-10000)
	$Sprite.visible = false
	boss_mode = mode.DIE
	
func check_collision():
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.name == "Player":
			var player = collision.collider
			player.die()
			break

func _process(delta):
	if die:
		return
	time += delta
	action(delta)
	if time > IDLE_PERIOD:
		if boss_mode == mode.NODE:
			get_parent().clear_nodes()
		if health <= max_health / 2:
			time = IDLE_PERIOD / 2
		else:
			time = 0
		time_node = 0
		boss_mode = mode.values()[1 + (randi() % 5)]
