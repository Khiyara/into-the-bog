# Gamejam Gamedev 2020

Rizkhi Pramudya Hastiputra
1706039660
Into the Bog
please bear in mind that this game project is developed in 1 week only, please forgive my spaghetti code.

# Free assets that used for this game
- Background: https://www.kenney.nl/assets/background-elements-redux
- Sprite: 
    - https://craftpix.net/freebies/free-2d-pirate-character-sprites/
    - https://www.gameart2d.com/temple-run---free-sprites.html
    - https://bevouliin.com/free-game-asset-blue-bat-sprites/
- Music: https://www.playonloop.com/royalty-free-music/video-game-chiptune-music/
