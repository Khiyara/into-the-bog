extends TextureRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
onready var scene_switcher = get_node("/root/Global")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Exit_gui_input(event):
	if event is InputEventMouseButton: 
		if not "Boss" in get_tree().get_current_scene().name:
			scene_switcher.save_game()
		get_tree().paused = false
		scene_switcher.goto_scene("res://Scenes/Mainmenu.tscn")
