extends Area2D

func _ready():
	pass # Replace with function body.

onready var scene_switcher = get_node("/root/Global")

func _on_Area2D_body_entered(body):
	var current_scene = get_tree().get_current_scene()
	if (body.name == "Player"):
		var context
		if body.last_checkpoint != null:
			context =  {"last_checkpoint": body.last_checkpoint, "last_scene": current_scene.get_name()}
		else:
			context = {"last_checkpoint": scene_switcher.get_param("last_checkpoint"), "last_scene": scene_switcher.get_param("last_scene") if scene_switcher.get_param("last_scene") != null else "Level1"}
		if (get_parent().name == "Level1"):
			print(context)
			context["Level1"] = body.last_checkpoint
			scene_switcher.goto_scene("res://Scenes/Level/Level2.tscn", context)
		elif (get_parent().name == "Level2"):
			print(context)
			context["Level2"] = body.last_checkpoint
			scene_switcher.goto_scene("res://Scenes/Level/Boss1.tscn", context)
		elif (get_parent().name == "Boss1"):
			print(context)
			context["Boss1"] = body.last_checkpoint
			scene_switcher.goto_scene("res://Scenes/Level/Level3.tscn", context)
		elif (get_parent().name == "Level3"):
			print(context)
			context["Level3"] = body.last_checkpoint
			scene_switcher.goto_scene("res://Scenes/Level/Level4.tscn", context)
		elif (get_parent().name == "Level4"):
			print(context)
			context["Level4"] = body.last_checkpoint
			scene_switcher.goto_scene("res://Scenes/Level/SaveLevel.tscn", context)
		elif (get_parent().name == "SaveLevel"):
			print(context)
			context["SaveLevel"] = body.last_checkpoint
			scene_switcher.goto_scene("res://Scenes/Level/Boss2.tscn", context)
		elif (get_parent().name == "Boss2"):
			print(context)
			scene_switcher.goto_scene("res://Scenes/WinScreen.tscn")
