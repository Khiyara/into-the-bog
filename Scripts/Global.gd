extends Node

var current_scene = null
var _params = null
var position = null

var level_1 = preload("res://Scenes/Level/Level1.tscn")

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	print("Current Scene: " + current_scene.name)
	position = level_1.instance().get_node("SpawnPoint").position
	OS.window_fullscreen = !OS.window_fullscreen

func get_param(name):
	if _params != null and _params.has(name):
		return _params[name]
	return null

func goto_scene(path, params=null):
	if params:
		_params = params
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.

	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:

	call_deferred("_deferred_goto_scene", path)


func _deferred_goto_scene(path):
	# It is now safe to remove the current scene
	if (current_scene):
		print("Deleting Scene: " + current_scene.name)
		current_scene.free()

	# Load the new scene.
	print(path)
	var s = ResourceLoader.load(path)

	# Instance the new scene.)
	current_scene = s.instance()
	print ("Current Scene: " + current_scene.name)

	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(current_scene)

	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)

func save_game():
	print("SAVING GAME")
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for node in save_nodes:
		if node.filename.empty():
			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
			continue
		if !node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue
		var node_data = node.call("save")
		save_game.store_line(to_json(node_data))
		
	print("DONE SAVING GAME")
	print(save_game)
	save_game.close()

func load_game():
	print("Try Loading Game")
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return false
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	save_game.open("user://savegame.save", File.READ)
	if (save_game.get_len() <= 0):
		return false
	while save_game.get_position() < save_game.get_len():
		var node_data = parse_json(save_game.get_line())
		print(node_data)

		#for i in node_data.keys():
		#	if i == "filename":
		#		continue
		#	print(i)
		#	self.get_node("/root/Player").set(i, node_data[i])
		var player = self.get_node("/root/Player")
		player.die_count = node_data["die"]
		player.boss_level_1_die = node_data["die_boss_1"]
		if node_data["name"] == "Player":
			self.get_node("/root/Player").unfreeze()
			var x = null
			var y = null
			if "x" in node_data.keys():
				x = node_data["x"]
			if "y" in node_data.keys():
				y = node_data["y"]
			var last_checkpoint = Vector2(x if x else 0, y if y else 0)
			var context = {"last_checkpoint": null, "last_scene": node_data["last_scene"] if node_data["last_scene"] != null else "Level1", "Level1": level_1.instance().get_node("SpawnPoint").position}
			self.goto_scene("res://Scenes/Level/" + context["last_scene"] + ".tscn", context)
	save_game.close()
	print("Loading game Success")
	return true
