extends TextureRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
onready var scene_switcher = get_node("/root/Global")
onready var level_1 = preload("res://Scenes/Level/Level1.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_TextureRect_gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		var player = self.get_node("/root/Player")
		player.unfreeze()
		player.die_count = 0
		player.boss_level_1_die = 0
		player.boss_level_2_die = 0
		player.last_checkpoint = null
		var context = {"last_checkpoint": level_1.instance().get_node("SpawnPoint").position, "last_scene": scene_switcher.get_param("last_scene"), "Level1": level_1.instance().get_node("SpawnPoint").position}
		print(context)
		scene_switcher.goto_scene("res://Scenes/Level/Level1.tscn", context)
