extends Label

func _ready():
	self.get_node("/root/Player").position = Vector2(-5000,-5000)
	self.get_node("/root/Player").freeze()
	self.text = "CONGRATULATION\nYOU BEAT THE GAME\nEASY ISN'T IT??\nIT ONLY TOOK\n" + str(self.get_node("/root/Player").die_count) + " DEADS"
