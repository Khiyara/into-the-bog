extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.hide()

var pause = false

func _process(delta):
	if not pause and Input.is_action_just_pressed("ui_cancel"):
		pause = true
		self.show()
		get_tree().paused = true
	elif pause and Input.is_action_just_pressed("ui_cancel"):
		pause = false
		get_tree().paused = false
		self.hide()
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
