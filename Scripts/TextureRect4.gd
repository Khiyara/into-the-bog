extends TextureRect

var playing = true

func _on_TextureRect_gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		if playing:
			playing = false
			self.get_node("/root/Player").get_node("BGM").stop()
		else:
			playing = true
			self.get_node("/root/Player").get_node("BGM").play()
