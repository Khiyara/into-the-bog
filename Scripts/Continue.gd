extends TextureRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass		

func _on_Continue_gui_input(event):
	if event is InputEventMouseButton: # Replace with function body.
		get_tree().paused = false
		var pause_node = self.get_parent().get_parent().get_parent()
		pause_node.pause = false
		pause_node.hide()
